Source: xdemorse
Section: hamradio
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               libasound2-dev,
               libgtk-3-dev
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://www.qsl.net/5b4az/pkg/morse/xdemorse/xdemorse.html
Vcs-Browser: https://salsa.debian.org/debian/xdemorse
Vcs-Git: https://salsa.debian.org/debian/xdemorse.git

Package: xdemorse
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: GUI to decode Morse signals to text
 X/GTK+ application for decoding Morse code signals into text. xdemorse
 detects the "dihs" and "dahs" that make a Morse code character via the
 computer's sound card, which can be connected to a radio receiver tuned
 to a CW Morse code transmission or to a tone generator.
 .
 The input signal is processed by a Goertzel tone detector algorithm which
 produces "mark" or "space" (signal/no signal) outputs and the resulting
 stream of Morse code "elements" is decoded into an ASCII character for
 printing to the Text viewer.
 .
 xdemorse has a certain level of tolerance towards operator errors (bad
 "fist") regarding deviation from the standard duration of the various
 elements that make up the Morse code.There is a "Waterfall" (audio
 spectrum) display derived from an integer-arithmetic FFT of the receiver's
 audio output.
 .
 This program has built-in CAT capability but only for the Yaesu FT847 or
 FT857 and Elecraft K2 or K3.
