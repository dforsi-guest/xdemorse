/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef DETECT_H
#define DETECT_H    1

#include "common.h"
#include "display.h"
#include "cat.h"

/* Minimum level of lead+trail edge of tone over
 * which the tone detector looks for mark or space */
#define CYCLES_PER_FRAG     2       /* Cycles of signal/signal fragment  */
#define SIGNAL_SCALE        45      /* Scale factor to fit signal in scope */
#define SQUELCH_MULTIPLIER  150     /* Multiplier for the squelch threshold */
#define RATIO_MULTIPLIER    20.0    /* Multiplier for the ratio threshold */

#endif

