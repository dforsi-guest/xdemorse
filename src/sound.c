/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "sound.h"
#include "shared.h"

/* ALSA pcm capture and mixer handles */
static snd_pcm_t *pcm_handle = NULL;
static snd_mixer_t *mixer_handle = NULL;
static snd_pcm_hw_params_t *hw_params = NULL;
static int playback_num_writes;

/*------------------------------------------------------------------------*/

/*  Setup_Sound_Card()
 *
 *  Sets up mixer and DSP devices
 */
  gboolean
Setup_Sound_Card( char *mesg, int *error )
{
  snd_mixer_elem_t *elem;
  snd_mixer_selem_id_t *sid;
  long cmin, cmax;


  /*** Set up pcm parameters ***/
  if( rc_data.open_type == SND_OPEN_CAPTURE )
  {
    /* Open pcm for capture */
    *error = snd_pcm_open(
        &pcm_handle, rc_data.snd_card,
        SND_PCM_STREAM_CAPTURE, SND_PCM_ASYNC );
  }
  else if( rc_data.open_type == SND_OPEN_PLAYBACK )
  {
    /* Open pcm for playback */
    *error = snd_pcm_open(
        &pcm_handle, rc_data.snd_card,
        SND_PCM_STREAM_PLAYBACK, SND_PCM_ASYNC );

    playback_num_writes = PLAYBACK_BUF_LEN / SND_PERIOD_SIZE;
    /* Allocate memory to Playback ring buffers */
    if( !playback_buf )
    {
      mem_alloc( (void **)&playback_buf, PLAYBACK_BUFFS * sizeof(short *) );
      size_t buf_size  = SND_NUM_CHANNELS * PLAYBACK_BUF_LEN * sizeof(short);
      for( int idx = 0; idx < PLAYBACK_BUFFS; idx++ )
      {
        /* Allocate playback buffer */
        playback_buf[idx] = NULL;
        mem_alloc( (void **)&playback_buf[idx], buf_size );
      }
    }
  } /* else if( rc_data.open_type == SND_OPEN_PLAYBACK ) */

  /* Abort on error */
  if( *error < 0 )
  {
    Strlcat( mesg, _("Cannot open audio device: "), MESG_SIZE );
    Strlcat( mesg, rc_data.snd_card, MESG_SIZE );
    return( FALSE );
  }

  /* Allocate memory to hardware parameters structure */
  *error = snd_pcm_hw_params_malloc( &hw_params );
  if( *error < 0 )
  {
    Strlcat( mesg, _("Cannot allocate hardware parameter struct"), MESG_SIZE );
    return( FALSE );
  }

  /* Initialize hardware parameter structure */
  *error = snd_pcm_hw_params_any( pcm_handle, hw_params );
  if( *error < 0 )
  {
    Strlcat( mesg, _("Cannot initialize hardware parameter struct"), MESG_SIZE );
    return( FALSE );
  }

  /* Set access type */
  *error = snd_pcm_hw_params_set_access(
      pcm_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED );
  if( *error < 0 )
  {
    Strlcat( mesg, _("Cannot set access type (RW_INTERLEAVED)"), MESG_SIZE );
    return( FALSE );
  }

  /* Set sample format */
  *error = snd_pcm_hw_params_set_format(
      pcm_handle, hw_params, SND_PCM_FORMAT_S16_LE );
  if( *error < 0 )
  {
    Strlcat( mesg, _("Cannot set sample format (S16_LE)"), MESG_SIZE  );
    return( FALSE );
  }

  /* Set sample rate */
  *error = snd_pcm_hw_params_set_rate( pcm_handle, hw_params,
      (unsigned int)rc_data.dsp_rate, SND_EXACT_VAL );
  if( *error < 0 )
  {
    snprintf( mesg, MESG_SIZE, _("Cannot set sample rate to %d"), rc_data.dsp_rate );
    return( FALSE );
  }

  /* Set channel count */
  *error = snd_pcm_hw_params_set_channels(
      pcm_handle, hw_params, SND_NUM_CHANNELS );
  if( *error < 0 )
  {
    snprintf( mesg, MESG_SIZE, _("Cannot set channel count to %d"), SND_NUM_CHANNELS );
    return( FALSE );
  }

  /* Set number of periods */
  *error = snd_pcm_hw_params_set_periods(
      pcm_handle, hw_params, SND_NUM_PERIODS, SND_EXACT_VAL );
  if( *error < 0)
  {
    snprintf( mesg, MESG_SIZE, _("Cannot set number of periods to %d"), SND_NUM_PERIODS );
    return( FALSE );
  }

  /* Set period size */
  *error = snd_pcm_hw_params_set_period_size(
      pcm_handle, hw_params, SND_PERIOD_SIZE, SND_EXACT_VAL );
  if( *error < 0)
  {
    snprintf( mesg, MESG_SIZE, _("Cannot set period size to %d"), SND_PERIOD_SIZE );
    return( FALSE );
  }

  /* Set parameters */
  *error = snd_pcm_hw_params( pcm_handle, hw_params );
  if( *error < 0 )
  {
    Strlcat( mesg, _("Cannot set capture parameters"), MESG_SIZE );
    return( FALSE );
  }
  snd_pcm_hw_params_free( hw_params );
  hw_params = NULL;

  /*** Set up mixer (capture source and volume) ***/
  if( rc_data.open_type == SND_OPEN_CAPTURE )
  {
    /* Open mixer */
    *error = snd_mixer_open( &mixer_handle, 0 );
    if( *error < 0 )
    {
      Strlcat( mesg, _("Cannot open mixer"), MESG_SIZE );
      return( FALSE );
    }

    /* Attach mixer */
    *error = snd_mixer_attach( mixer_handle, rc_data.snd_card );
    if( *error < 0 )
    {
      Strlcat( mesg, _("Cannot attach mixer to: "), MESG_SIZE );
      Strlcat( mesg, rc_data.snd_card,  MESG_SIZE );
      return( FALSE );
    }

    /* Register mixer */
    *error = snd_mixer_selem_register( mixer_handle, NULL, NULL );
    if( *error < 0 )
    {
      Strlcat( mesg, _("Cannot register mixer"), MESG_SIZE );
      return *error;
    }

    /* Load mixer */
    *error = snd_mixer_load( mixer_handle );
    if( *error < 0 )
    {
      Strlcat( mesg, _("Cannot load mixer"), MESG_SIZE );
      return( FALSE );
    }

    /* Allocate selem_id structure */
    *error = snd_mixer_selem_id_malloc( &sid );
    if( *error < 0 )
    {
      Strlcat( mesg, _("Cannot allocate selem_id struct"), MESG_SIZE );
      return( FALSE );
    }

    /* Find capture selem */
    snd_mixer_selem_id_set_index( sid, 0 );
    snd_mixer_selem_id_set_name( sid, rc_data.cap_src );
    elem = snd_mixer_find_selem( mixer_handle, sid );
    if( !elem )
    {
      Strlcat( mesg, _("Cannot find capture element: "), MESG_SIZE );
      Strlcat( mesg, rc_data.cap_src,  MESG_SIZE );
      snd_mixer_selem_id_free(sid);
      *error = 0;
      return( FALSE );
    }

    /* Set capture switch for cap source */
    if( snd_mixer_selem_has_capture_switch(elem) )
    {
      *error = snd_mixer_selem_set_capture_switch(
          elem, rc_data.channel, 1 );
      if( *error < 0 )
      {
        Strlcat( mesg, _("Cannot set capture device: "), MESG_SIZE );
        Strlcat( mesg, rc_data.cap_src,  MESG_SIZE );
        snd_mixer_selem_id_free(sid);
        return( FALSE );
      }
    }
    else
    {
      Strlcat( mesg, _("Device has no capture switch: "), MESG_SIZE );
      Strlcat( mesg, rc_data.cap_src,  MESG_SIZE );
      snd_mixer_selem_id_free(sid);
      *error = 0;
      return( FALSE );
    }

    /* Find capture volume selem */
    if( strcmp(rc_data.cap_vol, "--") != 0 )
    {
      snd_mixer_selem_id_set_index( sid, 0 );
      snd_mixer_selem_id_set_name( sid, rc_data.cap_vol );
      elem = snd_mixer_find_selem( mixer_handle, sid );
      if( elem == NULL )
      {
        Strlcat( mesg, _("Cannot find Volume element: "), MESG_SIZE );
        Strlcat( mesg, rc_data.cap_vol,  MESG_SIZE );
        Error_Dialog( mesg );
      }

      /* Set capture volume */
      if( elem != NULL )
      {
        if( snd_mixer_selem_has_capture_volume(elem) )
        {
          /* Change from % volume to sound card value */
          long lev;
          snd_mixer_selem_get_capture_volume_range( elem, &cmin, &cmax );
          lev = cmin + ((cmax - cmin) * rc_data.cap_level) / 100;

          /* Set capture volume */
          *error = snd_mixer_selem_set_capture_volume(
              elem, rc_data.channel, lev );
          if( *error < 0 )
          {
            snprintf( mesg, MESG_SIZE,
                _("Cannot set capture volume to %d\n"\
                  "Error: %s"),
                rc_data.cap_level, snd_strerror(*error) );
            Error_Dialog( mesg );
          }
        }
        else
        {
          Strlcat( mesg,
              _("Element has no Volume control: "), MESG_SIZE );
          Strlcat( mesg, rc_data.cap_vol,  MESG_SIZE );
          *error = 0;
          Error_Dialog( mesg );
        }
      } /* if( elem != NULL ) */

    } /* if( strcmp(rc_data.cap_vol, "--") != 0 ) */

    snd_mixer_selem_id_free(sid);
    Close_Mixer_Handle();
  }

  /* Prepare audio interface for use */
  *error = snd_pcm_prepare( pcm_handle );
  if( *error < 0 )
  {
    Strlcat( mesg,  _("Cannot prepare audio interface"), MESG_SIZE );
    return( FALSE );
  }

  return( TRUE );
} /* End of Setup_Sound_Card() */

/*------------------------------------------------------------------------*/

/*
 * These two functions close the sound card handles
 */
  void
Close_PCM_Handle( void )
{
  if( pcm_handle != NULL )
    snd_pcm_close( pcm_handle );
  pcm_handle = NULL;

  if( hw_params != NULL )
    snd_pcm_hw_params_free( hw_params );
  hw_params = NULL;
}

  void
Close_Mixer_Handle( void )
{
  if( mixer_handle != NULL )
    snd_mixer_close( mixer_handle );
  mixer_handle = NULL;
}

/*------------------------------------------------------------------------*/

/* Xrun_Recovery()
 *
 * Recover from underrun (broken pipe) and suspend
 */
  static gboolean
Xrun_Recovery( int error )
{
  if( error == -EPIPE )
  {
    error = snd_pcm_prepare( pcm_handle );
    if( error < 0 )
    {
      char mesg[MESG_SIZE];
      snprintf( mesg, sizeof(mesg),
          _("Cannot recover from underrun, prepare failed\n"\
            "Error: %s\n"), snd_strerror(error) );
      Error_Dialog( mesg );
      return( FALSE );
    }
  }
  else if( error == -ESTRPIPE )
  {
    while( (error = snd_pcm_resume(pcm_handle)) == -EAGAIN )
      sleep(1);
    if( error < 0 )
    {
      error = snd_pcm_prepare( pcm_handle );
      if( error < 0 )
      {
        char mesg[MESG_SIZE];
        snprintf( mesg, sizeof(mesg),
            _("Cannot recover from suspend, prepare failed\n"\
            "Error: %s\n"), snd_strerror(error) );
        Error_Dialog( mesg );
        return( FALSE );
      }
    }
  }

  return( TRUE );
} /* Xrun_Recovery() */

/*------------------------------------------------------------------------*/

/*  Get_Signal_Sample()
 *
 *  Gets the next DSP sample of the signal input
 */

  gboolean
Signal_Sample( short *sample )
{
  snd_pcm_sframes_t error;


  /* Refill dsp samples buffer when needed */
  if( samples_buffer.buffer_idx >= samples_buffer.buffer_size )
  {
    /* Start buffer index according to channel */
    samples_buffer.buffer_idx = rc_data.use_chnl;

    /* Read audio samples from DSP, abort on error */
    error = snd_pcm_readi(
        pcm_handle, samples_buffer.buffer, SND_READI_FRAMES );
    if( error != SND_READI_FRAMES )
    {
      char mesg[MESG_SIZE];
      snprintf( mesg, sizeof(mesg),
          _("Read from audio interface failed\n"\
            "Error: %s\n"), snd_strerror((int)error) );
      fprintf( stderr, "%s", mesg );

      /* Try to recover if broken pipe or suspend */
      if( !Xrun_Recovery((int)error) )
      {
        Cleanup();
        return( FALSE );
      }
    }
  } /* End of if( buffer_idx >= buffer_size ) */

  /* Get a sample from the buffer in short form
   * and do a IFFT when enough samples are collected */
  *sample = samples_buffer.buffer[samples_buffer.buffer_idx];
  IFFT_Data( *sample );

  /* Increment according to mono/stereo mode */
  samples_buffer.buffer_idx += SND_NUM_CHANNELS;

  return( TRUE );
} /* Signal_Sample() */

/*------------------------------------------------------------------------*/

/*  DSP_Write()
 *
 *  Write a samples buffer to DSP
 */
  gboolean
DSP_Write( short *buffer, int buf_len )
{
  snd_pcm_sframes_t error;
  int idx, buf_idx;

  /* Abort if sound card not ready */
  if( pcm_handle == NULL ) return( FALSE );

  /* Write samples buffer to DSP in chunks of PERIOD_SIZE */
  buf_idx = 0;
  for( idx = 0; idx < playback_num_writes; idx++ )
  {
    /* Write samples buffer to DSP */
    error = snd_pcm_writei( pcm_handle, &buffer[buf_idx], SND_PERIOD_SIZE );
    if( error != SND_PERIOD_SIZE )
    {
      if( error < 0 )
        fprintf( stderr, _("sdrx: DSP_Write(): %s\n"),
            snd_strerror((int)error) );
      else
        fprintf( stderr,
            _("sdrx: DSP_Write(): Written only %d frames out of %d\n"),
            (int)error, buf_len );

      /* Try to recover from error */
      if( !Xrun_Recovery( (int)error) ) return( FALSE );
    } /* if( error != PERIOD_SIZE ) */

     buf_idx += SND_BUF_STRIDE;
  } /* for( idx = 0; idx < playback_num_writes; idx++ ) */

  return( TRUE );
} /* DSP_Write() */

/*----------------------------------------------------------------------*/

