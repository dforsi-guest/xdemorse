/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "shared.h"

/*------------------------------------------------------------------------*/

/* Global widgets */
GtkWidget
  *rx_scrolledwindow,
  *scope,
  *waterfall,
  *scope_label;

/* Receiver type combobox */
GtkComboBoxText *receiver_combobox = NULL;

/* Speed and squelch spin button */
GtkSpinButton
  *frequency,
  *speed,
  *squelch,
  *ratio;

/* Main window builder */
GtkBuilder *main_builder = NULL;

/* Runtime config data */
rc_data_t rc_data;

/* DSP samples buffer */
samples_buffer_t samples_buffer;

/* Waterfall window pixbuf */
GdkPixbuf *wfall_pixbuf = NULL;
guchar *wfall_pixels;
gint
  wfall_rowstride,
  wfall_n_channels,
  wfall_width,
  wfall_height;

/* Text buffer for text viewer */
GtkTextBuffer *rx_text_buffer;

/* Scope signal display */
gint
  scope_width,
  scope_height;

/* Average bin values */
int *bin_ave = NULL;

/* IFFT data buffer*/
int16_t
  *ifft_data = NULL,
  ifft_data_length = 0;

sem_t pback_semaphore;

/* Sound Playback Buffer has to be a ring
 * buffer to allow for the differences in the
 * SDR ADC and Sound card ADC sampling rates */
short **playback_buf = NULL;
int   sound_buf_num;
int   demod_buf_num;

/*------------------------------------------------------------------------*/

