/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */


#include "display.h"
#include "shared.h"

/*------------------------------------------------------------------------*/

/* IFFT_Bin_Value()
 *
 * Calculates IFFT bin values with auto level control
 */
  static int
IFFT_Bin_Value( int sum_i, int sum_q, gboolean reset )
{
 /* Value of ifft output "bin" */
  static int bin_val = 0;

  /* Maximum value of ifft bins */
  static int bin_max = 1000, max = 0;


  /* Scale output from IFFT */
  sum_i /= IFFT_SCALE;
  sum_q /= IFFT_SCALE;

  /* Calculate sliding window average of max bin value */
  if( reset )
  {
    bin_max = max;
    if( !bin_max ) bin_max = 1;
    max = 0;
  }
  else
  {
    /* Calculate average signal power at each frequency (bin) */
    bin_val  = bin_val * AMPL_AVE_MUL;
    bin_val += sum_i * sum_i + sum_q * sum_q;
    bin_val /= AMPL_AVE_WIN;

    /* Record max bin value */
    if( max < bin_val ) max = bin_val;

    /* Scale bin values to 255 depending on max value */
    int ret = ( 255 * bin_val ) / bin_max;
    if( ret > 255 ) ret = 255;
    return( ret );
  }

  return( 0 );
} /* IFFT_Bin_Value() */

/*------------------------------------------------------------------------*/

/*  Display_Detector()
 *
 *  Updates the Detector scope display
 */

  void
Display_Detector( cairo_t *cr, GdkPoint *points )
{
  int idx;

  /* Draw scope background */
  cairo_set_source_rgb( cr, RGB_BACKGND );
  cairo_rectangle(
      cr, 0.0, 0.0,
      (double)scope_width  + 2.0,
      (double)scope_height + 2.0 );
  cairo_fill( cr );

  /* Plot signal graph */
  cairo_set_source_rgb( cr, RGB_GREEN );
  cairo_set_line_width( cr, CAIRO_LINE_WIDTH );
  cairo_move_to( cr, (double)points[0].x, (double)points[0].y );
  for( idx = 1; idx < scope_width; idx++ )
    cairo_line_to( cr, (double)points[idx].x, (double)points[idx].y );

  /* Stroke paths */
  cairo_stroke( cr );

  /* Draw Mark/Space threshold references */
  cairo_set_source_rgb( cr, RGB_WHITE );
  int temp = scope_height - rc_data.det_threshold;
  cairo_move_to( cr, 0.0, (double)temp );
  cairo_line_to( cr, (double)scope_width, (double)temp );

  /* Stroke paths */
  cairo_stroke( cr );

} /* Display_Detector() */

/*------------------------------------------------------------------------*/

/*  Display_Signal()
 *
 *  Updates the Signal scope display
 */

  void
Display_Signal( cairo_t *cr, GdkPoint *points )
{
  int idx;

  /* Draw scope background */
  cairo_set_source_rgb( cr, RGB_BACKGND );
  cairo_rectangle(
      cr, 0.0, 0.0,
      (double)scope_width  + 2.0,
      (double)scope_height + 2.0 );
  cairo_fill( cr );

  /* Plot signal graph */
  cairo_set_source_rgb( cr, RGB_GREEN );
  cairo_set_line_width( cr, CAIRO_LINE_WIDTH );
  cairo_move_to( cr, (double)points[0].x, (double)points[0].y );
  for( idx = 1; idx < scope_width; idx++ )
    cairo_line_to( cr, (double)points[idx].x, (double)points[idx].y );

  /* Stroke paths */
  cairo_stroke( cr );

} /* Display_Signal */

/*------------------------------------------------------------------------*/

/* Colorize()
 *
 * Pseudo-colorizes FFT Spectrum Display pixels
 * according to the IFFT's output bin values
 */
  static void
Colorize( guchar *pix, int pixel_val )
{
  int n;

  if( pixel_val < 64 ) // From black to blue
  {
    pix[0] = 0;
    pix[1] = 0;
    pix[2] = 3 + (guchar)pixel_val * 4;
  }
  else if( pixel_val < 128 ) // From blue to green
  {
    n = pixel_val - 127; // -63 <= n <= 0
    pix[0] = 0;
    pix[1] = 255 + (guchar)n * 4;
    pix[2] = 3   - (guchar)n * 4;
  }
  else if( pixel_val < 192 ) // From green to yellow
  {
    n = pixel_val - 191; // -63 <= n <= 0
    pix[0] = 255 + (guchar)n * 4;
    pix[1] = 255;
    pix[2] = 0;
  }
  else // From yellow to reddish-orange
  {
    n = pixel_val - 255; // -63 <= n <= 0
    pix[0] = 255;
    pix[1] = 66 - (guchar)n * 3;
    pix[2] = 0;
  }

} /* Colorize() */

/*----------------------------------------------------------------------*/

/* Display_Waterfall()
 *
 * Displays audio spectrum as "waterfall"
 */

  void
Display_Waterfall( void )
{
  int
    idh, idv,   /* Index to hor. and vert. position in warterfall */
    pixel_val,  /* Greyscale value of pixel derived from ifft o/p  */
    idf,        /* Index to ifft output array */
    idb,        /* Index to average bin values array */
    temp;

  /* Constant needed to draw center line in waterfall */
  int center_line = wfall_width / 2;

  /* Pointer to current pixel */
  static guchar *pix;


  /* Draw a vertical white line in waterfall at detector's freq. */
  pix    = wfall_pixels + wfall_rowstride + wfall_n_channels;
  pix   += wfall_n_channels * center_line;
  pix[0] = pix[1] = pix[2] = 0xff;

  /* Copy each line of waterfall to next one */
  temp = wfall_height - 2;
  for( idv = temp; idv > 0; idv-- )
  {
    pix = wfall_pixels + wfall_rowstride * idv + wfall_n_channels;

    for( idh = 0; idh < wfall_width; idh++ )
    {
      pix[0] = pix[ -wfall_rowstride];
      pix[1] = pix[1-wfall_rowstride];
      pix[2] = pix[2-wfall_rowstride];
      pix += wfall_n_channels;
    }
  }

  /* Go to top left +1 hor. +1 vert. of pixbuf */
  pix = wfall_pixels + wfall_rowstride + wfall_n_channels;

  /* First column of display (DC) not used */
  bin_ave[0] = 0;

  /* Do the IFFT on input array */
  IFFT_Real( ifft_data );
  idb = 0;
  for( idf = 2; idf < ifft_data_length; idf += 2 )
  {
    /* Calculate signal power at each frequency (bin) */
    pixel_val = IFFT_Bin_Value(
        ifft_data[idf], ifft_data[idf + 1], FALSE );

    /* Calculate average bin values */
    bin_ave[idb++] = pixel_val;

    /* Color code signal strength */
    Colorize( pix, pixel_val );
    pix += wfall_n_channels;

  } /* for( idf = 2; idf < ifft_data_length; idf += 2 ) */

  /* Reset function */
  IFFT_Bin_Value( 0, 0, TRUE );

  /* At last draw waterfall */
  gtk_widget_queue_draw( waterfall );

} /* Display_Waterfall() */

/*------------------------------------------------------------------------*/

/*  Print_Character()
 *
 *  Prints a character to a text view port
 */

  gboolean
Print_Character( gpointer data )
{
  /* Text buffer marker */
  static GtkTextIter iter;

  GtkAdjustment *adjustment;
  static gdouble last_upper = 0.0;

  /* Decoded Morse character */
  char dec_char[MAX_CHAR_LEN];


  /* Print decoded characters */
  dec_char[0] = '\0';
  if( Get_Character(dec_char) )
  {
    /* Return on 0 char */
    if( strlen(dec_char) == 0 ) return( TRUE );

    /* Print character */
    gtk_text_buffer_get_iter_at_offset( rx_text_buffer, &iter,
        gtk_text_buffer_get_char_count(rx_text_buffer) );

    gtk_text_buffer_insert( rx_text_buffer, &iter, (const gchar*)dec_char, -1 );

    /* Scroll Text View to bottom on change of page size */
    adjustment = gtk_scrolled_window_get_vadjustment
      ( GTK_SCROLLED_WINDOW(rx_scrolledwindow) );
    gdouble upper = gtk_adjustment_get_upper( adjustment );
    gdouble page_size = gtk_adjustment_get_page_size( adjustment );
    if( upper != last_upper )
    {
      gtk_adjustment_set_value( adjustment, upper - page_size );
      last_upper = page_size;
    }

    return( TRUE );
  } /* if( Get_Character(&dec_char) ) */

  return( FALSE );
} /* Print_Character() */

/*------------------------------------------------------------------------*/

