/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef COMMON_H
#define COMMON_H    1

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <semaphore.h>
#ifdef HAVE_LIBPERSEUS_SDR
  #include <perseus-sdr.h>
#endif

/* Definitions of flags for various actions */
#define ADAPT_SPEED      0x000001 /* Enable speed tracking */
#define MAN_THRESHOLD    0x000002 /* Enable manual detector threshold */
#define WORD_WRAP        0x000004 /* Enable word wrapping */
#define DISPLAY_SIGNAL   0x000008 /* Display output of signal detector */
#define DISPLAY_RATIO    0x000010 /* Display situation of Ratio threshold */
#define DISPLAY_LEVEL    0x000020 /* Display situation of Level threshold */
#define SCOPE_READY      0x000040 /* Scope data ready to display */
#define SCOPE_HOLD       0x000080 /* Hold scope display */
#define SELECT_LEVEL     0x000100 /* Select Level or Ratio situation display */
#define ENABLE_CAT       0x000200 /* Enable CAT for transceiver */
#define CAT_SETUP        0x000400 /* CAT is set up */
#define ENABLE_RECEIVE   0x000800 /* Enable reception of Morse code */
#define RCCONFIG_OK      0x001000 /* xdemorserc file read ok */
#define MARK_TONE        0x002000 /* Input signal is mark tone (key down) */
#define SPACE_TONE       0x004000 /* Input signal is space tone (key up)  */
#define TCVR_SERIAL_TEST 0x008000 /* Serial port is under test */
#ifdef HAVE_LIBPERSEUS_SDR
#define PERSEUS_INIT     0x010000 /* Perseus initialized OK */
#endif

/* Return values */
#define ERROR           1    /* Error condition */
#define SUCCESS         0    /* No error condition */

/* Size of char arrays (strings) for error messages etc */
#define MESG_SIZE   128

/*-------------------------------------------------------------------*/

/* Runtine configuration data */
typedef struct
{
  char
    snd_card[32], /* Sound card name */
    cap_src[32],  /* Capture source  */
    cap_vol[32],  /* Capture volume  */
    open_type;    /* Open PCM flag, for Playback or Capture */

  int
    channel,    /* ALSA "channel" for use in mixer setup */
    use_chnl,   /* Channel in use (frontleft=0, frontright=1 etc) */
    cap_level,  /* Recording/Capture level */
    dsp_rate;   /* DSP rate (speed) samples/sec */

  /* Transceiver serial device */
  char cat_serial[32];

  /* Receiver type 1=FT847 2=FT857 3=K2 4=K3 5=PERSEUS 0=NONE */
  int receiver_type;

  int
    receiver_freq, /* Receiver frequency in Hz */
    tone_freq,     /* Receiver BFO Tone freq */
    center_line,   /* Waterfall BFO Tone freq marker line */
    ifft_stride,   /* Stride of ifft over input samples   */
    speed_wpm,     /* Current Morse speed words/min   */
    unit_elem,     /* Morse unit element (dot) length */
    min_unit,      /* Minimum length of unit element  */
    max_unit,      /* Maximum length of unit element  */
    max_unit_x2,   /* Maximum length of unit element * 2 */
    det_squelch,   /* Squelch level of signal detector */
    det_threshold; /* Current threshold value of mark/space detector */

  double
    det_ratio,  /* Ratio of lead/trail edge for mark/space detection */
    freq_correction; /* Correction factor for crystal freq reference */

  /* Glade file */
  char xdemorse_glade[64];

} rc_data_t;

/* Filter data struct */
typedef struct filter_data
{
  double
    cutoff, /* Cutoff frequency as fraction of sample rate */
    ripple; /* Passband ripple as a percentage */

  int
    npoles, /* Number of poles, _must_ be even */
    type;   /* Filter type, as below this struct */

  /* a and b Coefficients of the filter */
  double *a, *b;

  /* Saved input and output values */
  double *x, *y;

  /* Ring buffer index for above */
  int ring_idx;

  /* Input samples buffer and its length */
  double *samples_buf;
  int samples_buf_len;

} filter_data_t;

/* Filter type for above struct */
enum
{
  FILTER_LOWPASS = 0,
  FILTER_HIGHPASS,
  FILTER_BANDPASS
};

typedef struct
{
  /* Signal/dsp samples buffer */
  short *buffer;

  int
    buffer_idx,  /* Index to signal samples buffer */
    buffer_size; /* Buffer size according to stereo/mono mode */

} samples_buffer_t;

/*-------------------------------------------------------------------*/

/* Should have been in math.h */
#ifndef M_2PI
  #define M_2PI     6.28318530717958647692
#endif

#ifndef M_PI
  #define M_PI      3.14159265358979323846
#endif

/*-------------------------------------------------------------------*/

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  define Q_(String) g_strip_context ((String), gettext (String))
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define Q_(String) g_strip_context ((String), (String))
#  define N_(String) (String)
#endif

/*-------------------------------------------------------------------*/

/* Function prototypes produced by cproto */
/* callbacks.c */
void Error_Dialog(char *message);
void on_main_window_destroy(GObject *object, gpointer user_data);
gboolean on_main_window_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data);
gboolean on_scope_drawingarea_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data);
void on_receiver_combobox_changed(GtkComboBox *combobox, gpointer user_data);
void on_frequency_spinbutton_value_changed(GtkSpinButton *spin_button, gpointer user_data);
gboolean on_frequency_spinbutton_scroll_event(GtkWidget *widget, GdkEvent *event, gpointer user_data);
void on_wpm_spinbutton_value_changed(GtkSpinButton *spin_button, gpointer user_data);
void on_squelch_spinbutton_value_changed(GtkSpinButton *spin_button, gpointer user_data);
void on_ratio_spinbutton_value_changed(GtkSpinButton *spin_button, gpointer user_data);
void on_error_quit_button_clicked(GtkButton *button, gpointer user_data);
void on_clear_button_clicked(GtkButton *button, gpointer user_data);
gboolean on_waterfall_drawingarea_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data);
gboolean on_waterfall_drawingarea_button_press_event(GtkWidget *widget, GdkEventButton *event, gpointer user_data);
void on_error_ok_button_clicked(GtkButton *button, gpointer user_data);
void on_auto_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data);
void on_error_dialog_destroy(GObject *object, gpointer user_data);
gboolean on_error_dialog_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data);
void on_waterfall_drawingarea_configure_event(GtkWidget *widget, GdkEventConfigure *event, gpointer user_data);
void on_receive_togglebutton_toggled(GtkToggleButton *togglebutton, gpointer user_data);
void on_ratio_radiobutton_toggled(GtkToggleButton *togglebutton, gpointer user_data);
void on_level_radiobutton_toggled(GtkToggleButton *togglebutton, gpointer user_data);
void on_stop_radiobutton_toggled(GtkToggleButton *togglebutton, gpointer user_data);
void on_signal_radiobutton_toggled(GtkToggleButton *togglebutton, gpointer user_data);
/* cat.c */
gboolean Write_Rx_Freq(int freq);
gboolean Open_Tcvr_Serial(void);
void Close_Tcvr_Serial(void);
gboolean Tune_Tcvr(double x);
/* decode.c */
gboolean Get_Character(char *chr);
/* detect.c */
gboolean Get_Fragment(void);
void Display_Scope(cairo_t *cr);
gboolean Initialize_Detector(void);
/* display.c */
void Display_Detector(cairo_t *cr, GdkPoint *points);
void Display_Signal(cairo_t *cr, GdkPoint *points);
void Display_Waterfall(void);
gboolean Print_Character(gpointer data);
/* filters.c */
void Init_Chebyshev_Filter(filter_data_t *filter_data);
void DSP_Filter(filter_data_t *filter_data);
/* ifft.c */
gboolean Initialize_IFFT(int16_t width);
void IFFT_Real(int16_t *data);
void IFFT_Data(int16_t sample);
/* interface.c */
GtkWidget *Builder_Get_Object(GtkBuilder *builder, gchar *name);
GtkWidget *create_main_window(GtkBuilder **builder);
GtkWidget *create_error_dialog(GtkBuilder **builder);
/* main.c */
int main(int argc, char *argv[]);
void Usage(void);
/* perseus.c */
#ifdef HAVE_LIBPERSEUS_SDR
gboolean Demodulate_CW(short *signal_sample);
void Perseus_Set_Center_Frequency(int center_freq);
void Perseus_Close_Device(void);
gboolean Perseus_Initialize(void);
#endif
/* shared.c */
/* sound.c */
gboolean Setup_Sound_Card(char *mesg, int *error);
void Close_PCM_Handle(void);
void Close_Mixer_Handle(void);
gboolean Signal_Sample(short *sample);
gboolean DSP_Write(short *buffer, int buf_len);
/* utils.c */
int isFlagSet(int flag);
int isFlagClear(int flag);
void Set_Flag(int flag);
void Clear_Flag(int flag);
void Toggle_Flag(int flag);
void Cleanup(void);
gboolean mem_alloc(void **ptr, size_t req);
gboolean mem_realloc(void **ptr, size_t req);
void free_ptr(void **ptr);
void Strlcpy(char *dest, const char *src, size_t n);
void Strlcat(char *dest, const char *src, size_t n);
gboolean Load_Config(gpointer data);
void Waterfall_Configure_Event(int width, int height);
int fix_fft( short reX[], short imX[], short fft_order, short inverse );
int fix_fftr( short realX[], int16_t fft_order, int16_t inverse );

#endif

