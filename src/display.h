/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef DISPLAY_H
#define DISPLAY_H   1

#include "common.h"
#include "decode.h"

/* Length and multiplier of amplitude averaging window  */
#define AMPL_AVE_WIN        3
#define AMPL_AVE_MUL        2

/* Some colors used in plotting */
#define RGB_GREEN       0.0, 1.0, 0.0
#define RGB_BACKGND     0.0, 0.2, 0.0
#define RGB_WHITE       1.0, 1.0, 1.0

#define CAIRO_LINE_WIDTH    1.5

/* Scale FFT output bins */
#define IFFT_SCALE      64

#endif

